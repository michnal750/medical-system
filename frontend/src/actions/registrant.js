import axiosDefault from "./axiosConfiguration";

function getAppointments(date) {
  return axiosDefault({
    method: "GET",
    url: "api/appointment/registrant",
    params: {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear()
    }
  });
}

function addAppointment(data) {
  return axiosDefault({
    method: "POST",
    url: "api/appointment/registrant",
    data: data,
  });
}

export { getAppointments, addAppointment};
