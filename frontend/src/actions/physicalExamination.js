import axiosDefault from "./axiosConfiguration";

function addPhysicalTest(data) {
  return axiosDefault({
    method: "POST",
    url: "/api/physicalExamination/doctor",
    data,
  });
}

function getPatientPhysicalTests(id) {
  return axiosDefault({
    method: "GET",
    url: `/api/physicalExamination/doctor/patient/${id}`,
  })
}

export { addPhysicalTest, getPatientPhysicalTests };
