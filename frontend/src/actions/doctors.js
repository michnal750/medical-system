import axiosDefault from "./axiosConfiguration";

function getAppointments(date, statusCode) {
  return axiosDefault({
    method: "GET",
    url: "/api/appointment/doctor",
    params: {
      day: date.getDate(),
      month: date.getMonth() + 1,
      year: date.getFullYear(),
      statusCode,
    }
  });
}

function getDoctors() {
  return axiosDefault({
    method: "GET",
    url: '/api/doctor/registrant',
  });
}

export { getAppointments, getDoctors };
