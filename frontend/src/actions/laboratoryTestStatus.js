import axiosDefault from "./axiosConfiguration";

function getLaboratoryTestStatuses() {
  return axiosDefault({
    method: "GET",
    url: "/api/laboratoryTestStatus",
  });
}

export { getLaboratoryTestStatuses };
