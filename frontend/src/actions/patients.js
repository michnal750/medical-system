import axiosDefault from "./axiosConfiguration";

function getPatients() {
  return axiosDefault({
    method: "GET",
    url: '/api/patient/registrant',
  });
}

function addPatient(data) {
  return axiosDefault({
    method: "POST",
    url: '/api/patient/registrant',
    data,
  })
}

export { getPatients, addPatient };
