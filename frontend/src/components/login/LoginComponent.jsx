import React from 'react';
import { Card, TextField, Grid, Typography, Button } from '@material-ui/core'
import { withFormik } from 'formik';
import * as Yup from "yup";
import { withSnackbar } from './../../ui/SnackbarContext';
import LoadingOverlay from './../../ui/LoadingOverlay';

const formikEnhancer = withFormik({
  enableReinitialize: true,

  mapPropsToValues: props => ({
    Login: '',
    Password: '',
  }),

  handleSubmit: (values, { props }) => {
    props.log(values)
      .then(() => {
        props.redirectToMainPage();
      })
      .catch(error => {
        props.disableOverlay();
        if (error.response && error.response.data) {
          props.showMessage(error.response.data);
        } else {
          props.showMessage('Nieznany błąd');
        }
      });
  },

  validationSchema: Yup.object().shape({
    Login: Yup.string().required("Login nie może być pusty!"),
    Password: Yup.string().required("Hasło nie może być puste!").nullable()
  })
});

const LoginComponent = (props) => {

  const {
    touched,
    errors,
  } = props;

  return (
  <>
    <Card className='login-card box-center'>
      <Typography variant="h5" className="underline-title-login">
        medical system
      </Typography>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              className='input'
              id='Login'
              onChange={props.handleChange}
              fullWidth
              variant='outlined'
              label='Login'
              style={{ marginTop: '1%' }}
              value={props.values.Login}
              helperText={touched.Login ? errors.Login : ""}
              error={touched.Login && Boolean(errors.Login)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              className='input'
              id='Password'
              onChange={props.handleChange}
              fullWidth
              variant='outlined'
              label='Hasło'
              type="password"
              value={props.values.Password}
              helperText={touched.Password ? errors.Password : ""}
              error={touched.Password && Boolean(errors.Password)}
            />
          </Grid>
          <Grid item xs={0} md={6} />
          <Grid item xs={12} md={6}>
            <Button color="primary" fullWidth onClick={props.handleSubmit} type="submit">
              Zaloguj
        </Button>
          </Grid>
        </Grid>
      </form>
    </Card>
    <LoadingOverlay
      open={props.isLoading}
    />
  </>
  )
};

export default withSnackbar(formikEnhancer(LoginComponent));