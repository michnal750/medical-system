import React from 'react';
import { Typography, Card, TextField, Select, Grid, Divider, MenuItem, Button, InputLabel } from '@material-ui/core';
import { withFormik } from 'formik';
import * as Yup from "yup";
import { withSnackbar } from '../../../ui/SnackbarContext';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import BackspaceIcon from '@material-ui/icons/Backspace';
import LoadingOverlay from './../../../ui/LoadingOverlay';


const formikEnhancer = withFormik({
  enableReinitialize: true,

  mapPropsToValues: props => ({
    Login: props.editMode && props.user && props.user.login ? props.user.login : '',
    Password: null,
    FirstName: props.editMode && props.user && props.user.firstName ? props.user.firstName : '',
    Surname: props.editMode && props.user && props.user.surname ? props.user.surname : '',
    Role: props.editMode && props.user && props.user.role ? props.user.role.code : 'ADM',
    LicenceNumber: props.editMode && props.user && props.user.licenceNumber ? props.user.licenceNumber : '',
  }),

  handleSubmit: (values, { props }) => {
    const code = values.Role;
    let data = { ...values, Role: { Code: code }, IsActive: true };
    let onSubmit = props.onEdit;
    if (!props.editMode) {
      onSubmit = props.onAdd;
    }

    onSubmit(data)
      .then(res => {
        props.showMessage(res.data);
        props.goBack();
      })
      .catch(error => {
        if (error.response) {
          props.showMessage(error.response.data);
        } else {
          props.showMessage("Nieznany błąd");
        }
      });
  },

  validationSchema: Yup.object().shape({
    Login: Yup.string().required("Login nie może być pusty!"),
    Password: Yup.string().required("Hasło nie może być puste!").nullable(),
    FirstName: Yup.string().required("Imię nie może być puste!"),
    Surname: Yup.string().required("Nazwisko nie może być puste!"),
    Role: Yup.string().required("Rola nie może być pusta!"),
    LicenceNumber: Yup.string().when('Role', {
      is: 'DOC',
      then: Yup.string().required('Numer licencji nie może byc pusty!'),
      otherwise: Yup.string()
    })
  }),

  validateOnBlur: false
});

const AddUserComponent = (props) => {

  const {
    touched,
    errors,
  } = props;

  return (
    <Card className="card">
      <Typography variant="h5" className="underline-title">
      {props.editMode ? 'Edytuj użytkownika' : 'Dodaj użytkownika'}
      </Typography>

      <Grid container spacing={2}>

        <Grid item xs={12} sm={3}>
          <InputLabel>
            Login
          </InputLabel>
          <TextField
            id='Login'
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            fullWidth
            variant='outlined'
            value={props.values.Login}
            helperText={touched.Login ? errors.Login : ""}
            error={touched.Login && Boolean(errors.Login)}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <InputLabel>
            Hasło
          </InputLabel>
          <TextField
            id='Password'
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            fullWidth
            variant='outlined'
            value={props.values.Password}
            type="password"
            helperText={touched.Password ? errors.Password : ""}
            error={touched.Password && Boolean(errors.Password)}
          />
        </Grid>
      </Grid>

      <Divider style={{ marginBottom: '2%', marginTop: "2%" }} />

      <Grid container spacing={2}>
        <Grid item xs={12} sm={3}>
          <InputLabel>
            Imię
          </InputLabel>
          <TextField
            id='FirstName'
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            fullWidth
            variant='outlined'
            value={props.values.FirstName}
            helperText={touched.FirstName ? errors.FirstName : ""}
            error={touched.FirstName && Boolean(errors.FirstName)}
          />
        </Grid>

        <Grid item xs={12} sm={3}>
          <InputLabel>
            Nazwisko
          </InputLabel>
          <TextField
            id='Surname'
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            fullWidth
            variant='outlined'
            value={props.values.Surname}
            helperText={touched.Surname ? errors.Surname : ""}
            error={touched.Surname && Boolean(errors.Surname)}
          />
        </Grid>
      </Grid>
      <Divider style={{ marginBottom: '2%', marginTop: "2%" }} />

      <Grid container spacing={2}>

        <Grid item xs={12} sm={3}>
          <InputLabel>
            Rola
          </InputLabel>
          <TextField
            id="Role"
            value={props.values.Role}
            onChange={props.handleChange('Role')}
            onBlur={props.handleBlur('Role')}
            variant="outlined"
            fullWidth
            disabled={props.editMode}
            select
          >
            {props.roles.map((element) => (
              <MenuItem value={element.code}>
                {element.name}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item xs={12} sm={3}>
          <InputLabel>
            Numer licencji
          </InputLabel>
          <TextField
            id='LicenceNumber'
            onChange={props.handleChange}
            onBlur={props.handleBlur}
            fullWidth
            variant='outlined'
            value={props.values.LicenceNumber}
            disabled={props.values.Role !== 'DOC'}
            helperText={touched.LicenceNumber ? errors.LicenceNumber : ""}
            error={touched.LicenceNumber && Boolean(errors.LicenceNumber)}
          />
        </Grid>

      </Grid>

      <Divider style={{ marginBottom: '2%', marginTop: "2%" }} />

      <div style={{ textAlign: 'right' }} >
        <div>
          <Button
            color="primary"
            variant="outlined"
            style={{ marginRight: '1%' }}
            endIcon={<BackspaceIcon />}
            onClick={props.goBack}
          >
            Wróć
          </Button>
          <Button
            color="primary"
            variant="outlined"
            endIcon={<SaveIcon />}
            onClick={props.handleSubmit}
          >
            {props.editMode ? 'Zapisz' : 'Dodaj'}
          </Button>
        </div>
      </div>
      <LoadingOverlay
        open={props.isLoading}
      />
    </Card >

  );
}

export default withSnackbar(formikEnhancer(AddUserComponent));