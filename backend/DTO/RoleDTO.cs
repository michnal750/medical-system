﻿namespace MedicalSystemBackend.DTO
{
    public class RoleDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}