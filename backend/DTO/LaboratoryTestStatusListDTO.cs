﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class LaboratoryTestStatusListDTO
    {
        public List<LaboratoryTestStatusDTO> Statuses { get; set; }
        public LaboratoryTestStatusListDTO()
        {
            Statuses = new List<LaboratoryTestStatusDTO>();
        }
    }
}
