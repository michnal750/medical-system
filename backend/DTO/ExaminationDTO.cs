﻿namespace MedicalSystemBackend.DTO
{
    public class ExaminationDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string TypeCode { get; set; }
    }
}
