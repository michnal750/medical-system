﻿namespace MedicalSystemBackend.DTO
{
    public class PhysicalExaminationDTO
    {
        public int Id { get; set; }
        public string Result { get; set; }
        public int AppointmentId { get; set; }
        public string ExaminationCode { get; set; }
        public string RegistrationDate { get; set; }
        public string FinishedCancelledDate { get; set; }
        public string Code { get; set; }
    }
}
