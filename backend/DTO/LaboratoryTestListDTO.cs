﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class LaboratoryTestListDTO
    {
        public List<LaboratoryTestDTO> LaboratoryTests { get; set; }
        public LaboratoryTestListDTO()
        {
            LaboratoryTests = new List<LaboratoryTestDTO>();
        }
    }
}
