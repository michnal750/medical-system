﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class PatientListDTO
    {
        public List<PatientDTO> Patients { get; set; }
        public PatientListDTO()
        {
            Patients = new List<PatientDTO>();
        }
    }
}
