﻿namespace MedicalSystemBackend.DTO
{
    public class LaboratoryTestDTO
    {
        public int Id { set; get; }
        public string DoctorNote { set; get; }
        public string SupervisorNote { set; get; }
        public string OrderedDate { set; get; }
        public int AppointmentId { set; get; }
        public string ExaminationCode { set; get; }
        public string? ExecutionCancelledDate { set; get; }
        public string? AcceptionCancelledDate { set; get; }
        public string Result { get; set; }
        public int? LaboratoryAssistantId { get; set; }
        public int? LaboratorySupervisorId { get; set; }
        public string StatusCode { set; get; }
    }
}
