﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IUserRoleService
    {
        RoleListDTO GetRoles();
    }
}
