﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface ILaboratoryTestService
    {
        LaboratoryTestListDTO GetLaboratoryTests();
        LaboratoryTestListDTO GetLaboratoryTestsByStatus(string status);
        void AddLaboratoryTest(LaboratoryTestDTO newLaboratoryTestDTO);
        LaboratoryTestDTO GetLaboratoryTestById(int testId);
        void MakeCancelLaboratoryTest(int testId, MakeAcceptCancelLaboratoryTestDTO makeLaboratoryTestDTO, int laboratoryAssistantId, string status);
        void AcceptCancelLaboratoryTest(int testId, MakeAcceptCancelLaboratoryTestDTO acceptLaboratoryTestDTO, int laboratorySupervisorId, string status);
        LaboratoryTestListDTO GetLaboratoryTestByPatientId(int patientId);
    }
}
