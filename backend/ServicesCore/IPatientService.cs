﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IPatientService
    {
        PatientListDTO GetPatients();
        void AddPatient(PatientDTO newPatientDTO);
        bool CheckIfPersonalIdentityNumberIsUnique(string PersonalIdentityNumber);
    }
}
