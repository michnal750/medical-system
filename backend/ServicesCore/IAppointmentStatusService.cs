﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IAppointmentStatusService
    {
        AppointmentStatusListDTO GetStatuses();
        string GetCodeByName(string statusName);
    }
}
