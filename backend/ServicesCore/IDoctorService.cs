﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IDoctorService
    {
        DoctorListDTO GetDoctors();
    }
}
