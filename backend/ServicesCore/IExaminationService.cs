﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IExaminationService
    {
        ExaminationListDTO getExaminationsByType(string type);
    }
}
