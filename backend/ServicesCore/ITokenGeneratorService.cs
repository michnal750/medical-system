﻿
namespace MedicalSystemBackend.ServicesCore
{
    public interface ITokenGeneratorService
    {
        string GenerateToken(int userId);
    }
}