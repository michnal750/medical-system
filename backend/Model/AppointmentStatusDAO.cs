﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class AppointmentStatusDAO
    {
        [Key]
        [Column(TypeName = "varchar(5)")]
        public string Code { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }

        public List<AppointmentDAO> Appointments { get; set; }
    }
}
