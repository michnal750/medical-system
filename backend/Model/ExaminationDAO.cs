﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class ExaminationDAO
    {
        [Key]
        [Column(TypeName = "varchar(50)")]
        public string Code { get; set; }
        public string Name { get; set; }

        [Required]
        public string TypeCode { get; set; }
        public ExaminationTypeDAO Type { get; set; }
    }
}
