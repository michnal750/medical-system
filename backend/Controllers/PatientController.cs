﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/patient")]
    public class PatientController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IPatientService patientService;

        public PatientController(
            IUserProviderService userProviderService,
            IUserService userService,
            IPatientService patientService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.patientService = patientService;
        }

        [Authorize]
        [HttpGet("registrant")]
        public IActionResult GetPatients()
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsRegistrant(userId))
            {
                PatientListDTO patientsListDTO = patientService.GetPatients();
                return Ok(patientsListDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien rejestratora");
            }
        }

        [Authorize]
        [HttpPost("registrant")]
        public IActionResult AddPatient([FromBody] PatientDTO newPatientDTO)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsRegistrant(userId))
            {
                try
                {
                    patientService.AddPatient(newPatientDTO);
                    return Ok("Pacjent zostal dodany pomyslnie");
                }
                catch (MedicalSystemServerException exception)
                {
                    return BadRequest(exception.Message);
                }
            }
            else
            {
                return BadRequest("Brak uprawnien rejestratora");
            }
        }
    }
}
