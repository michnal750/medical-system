﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/examination")]
    public class ExaminationController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IExaminationService examinationService;

        public ExaminationController(IUserProviderService userProviderService,
        IUserService userService,
        IExaminationService examinationService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.examinationService = examinationService;
        }

        [Authorize]
        [HttpGet("doctor")]
        public IActionResult GetExaminations([FromQuery] string type)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                ExaminationListDTO examinations = examinationService.getExaminationsByType(type);
                return Ok(examinations);
            }
            else
            {
                return BadRequest("Brak uprawnien doktora");
            }
        }
    }
}
