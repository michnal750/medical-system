﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/appointmentStatus")]
    public class AppointmentStatusController : ControllerBase
    {
        private readonly IAppointmentStatusService appointmentStatusService;

        public AppointmentStatusController(IAppointmentStatusService appointmentStatusService)
        {
            this.appointmentStatusService = appointmentStatusService;
        }

        [Authorize]
        [HttpGet("")]
        public IActionResult getAppointmentStatuses()
        {
            AppointmentStatusListDTO appointmentStatusListDTO = appointmentStatusService.GetStatuses();
            return Ok(appointmentStatusListDTO);
        }
    }
}
