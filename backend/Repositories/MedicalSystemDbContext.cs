﻿using MedicalSystemBackend.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace MedicalSystemBackend.Repositories
{
    public class MedicalSystemDbContext : DbContext
    {
        public DbSet<AppointmentDAO> Appointments { get; set; }
        public DbSet<LaboratoryTestDAO> LaboratoryTests { get; set; }
        public DbSet<PhysicalExaminationDAO> PhysicalExaminations { get; set; }
        public DbSet<ExaminationDAO> Examinations { get; set; }
        public DbSet<AppointmentStatusDAO> AppointmentStatuses { get; set; }
        public DbSet<DoctorDAO> Doctors { get; set; }
        public DbSet<ExaminationTypeDAO> ExaminationTypes { get; set; }
        public DbSet<LaboratoryAssistantDAO> LaboratoryAssistants { get; set; }
        public DbSet<LaboratorySupervisorDAO> LaboratorySupervisors { get; set; }
        public DbSet<LaboratoryTestStatusDAO> LaboratoryTestStatuses { get; set; }
        public DbSet<PatientDAO> Patients { get; set; }
        public DbSet<RegistrantDAO> Registrants { get; set; }
        public DbSet<UserDAO> Users { get; set; }
        public DbSet<AdminDAO> Admins { get; set; }
        public DbSet<UserRoleDAO> UserRoles { get; set; }

        public MedicalSystemDbContext(DbContextOptions<MedicalSystemDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            foreach (var foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
