﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Repositories
{
    public interface IRepository<T, M> where T : class
    {
        T Get(M Id);
        IEnumerable<T> GetAll();
        T Delete(M Id);
        T Add(T tObject);
        T Update(T tObject);
    }
}
