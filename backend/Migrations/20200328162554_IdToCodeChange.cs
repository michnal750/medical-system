﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalSystemBackend.Migrations
{
    public partial class IdToCodeChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointments_AppointmentStatuses_StatusId",
                table: "Appointments");

            migrationBuilder.DropForeignKey(
                name: "FK_Examinations_ExaminationTypes_TypeId",
                table: "Examinations");

            migrationBuilder.DropForeignKey(
                name: "FK_LaboratoryTests_LaboratoryTestStatuses_StatusId",
                table: "LaboratoryTests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LaboratoryTestStatuses",
                table: "LaboratoryTestStatuses");

            migrationBuilder.DropIndex(
                name: "IX_LaboratoryTests_StatusId",
                table: "LaboratoryTests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExaminationTypes",
                table: "ExaminationTypes");

            migrationBuilder.DropIndex(
                name: "IX_Examinations_TypeId",
                table: "Examinations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AppointmentStatuses",
                table: "AppointmentStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Appointments_StatusId",
                table: "Appointments");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "LaboratoryTestStatuses");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "LaboratoryTests");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ExaminationTypes");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "AppointmentStatuses");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Appointments");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "LaboratoryTestStatuses",
                type: "varchar(5)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "StatusCode",
                table: "LaboratoryTests",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "ExaminationTypes",
                type: "varchar(5)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TypeCode",
                table: "Examinations",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "AppointmentStatuses",
                type: "varchar(5)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "StatusCode",
                table: "Appointments",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LaboratoryTestStatuses",
                table: "LaboratoryTestStatuses",
                column: "Code");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExaminationTypes",
                table: "ExaminationTypes",
                column: "Code");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AppointmentStatuses",
                table: "AppointmentStatuses",
                column: "Code");

            migrationBuilder.CreateIndex(
                name: "IX_LaboratoryTests_StatusCode",
                table: "LaboratoryTests",
                column: "StatusCode");

            migrationBuilder.CreateIndex(
                name: "IX_Examinations_TypeCode",
                table: "Examinations",
                column: "TypeCode");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_StatusCode",
                table: "Appointments",
                column: "StatusCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointments_AppointmentStatuses_StatusCode",
                table: "Appointments",
                column: "StatusCode",
                principalTable: "AppointmentStatuses",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Examinations_ExaminationTypes_TypeCode",
                table: "Examinations",
                column: "TypeCode",
                principalTable: "ExaminationTypes",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LaboratoryTests_LaboratoryTestStatuses_StatusCode",
                table: "LaboratoryTests",
                column: "StatusCode",
                principalTable: "LaboratoryTestStatuses",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Appointments_AppointmentStatuses_StatusCode",
                table: "Appointments");

            migrationBuilder.DropForeignKey(
                name: "FK_Examinations_ExaminationTypes_TypeCode",
                table: "Examinations");

            migrationBuilder.DropForeignKey(
                name: "FK_LaboratoryTests_LaboratoryTestStatuses_StatusCode",
                table: "LaboratoryTests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LaboratoryTestStatuses",
                table: "LaboratoryTestStatuses");

            migrationBuilder.DropIndex(
                name: "IX_LaboratoryTests_StatusCode",
                table: "LaboratoryTests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExaminationTypes",
                table: "ExaminationTypes");

            migrationBuilder.DropIndex(
                name: "IX_Examinations_TypeCode",
                table: "Examinations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AppointmentStatuses",
                table: "AppointmentStatuses");

            migrationBuilder.DropIndex(
                name: "IX_Appointments_StatusCode",
                table: "Appointments");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "LaboratoryTestStatuses");

            migrationBuilder.DropColumn(
                name: "StatusCode",
                table: "LaboratoryTests");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "ExaminationTypes");

            migrationBuilder.DropColumn(
                name: "TypeCode",
                table: "Examinations");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "AppointmentStatuses");

            migrationBuilder.DropColumn(
                name: "StatusCode",
                table: "Appointments");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "LaboratoryTestStatuses",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "LaboratoryTests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "ExaminationTypes",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "AppointmentStatuses",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Appointments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_LaboratoryTestStatuses",
                table: "LaboratoryTestStatuses",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExaminationTypes",
                table: "ExaminationTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AppointmentStatuses",
                table: "AppointmentStatuses",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_LaboratoryTests_StatusId",
                table: "LaboratoryTests",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Examinations_TypeId",
                table: "Examinations",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_StatusId",
                table: "Appointments",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Appointments_AppointmentStatuses_StatusId",
                table: "Appointments",
                column: "StatusId",
                principalTable: "AppointmentStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Examinations_ExaminationTypes_TypeId",
                table: "Examinations",
                column: "TypeId",
                principalTable: "ExaminationTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_LaboratoryTests_LaboratoryTestStatuses_StatusId",
                table: "LaboratoryTests",
                column: "StatusId",
                principalTable: "LaboratoryTestStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
