﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{
    public class DoctorService : IDoctorService
    {
        private readonly IRepository<DoctorDAO, int> doctorsRepository;
        private readonly IRepository<UserDAO, int> usersRepository;

        public DoctorService(
            IRepository<DoctorDAO, int> doctorsRepository,
            IRepository<UserDAO, int> usersRepository)
        {
            this.doctorsRepository = doctorsRepository;
            this.usersRepository = usersRepository;
        }

        public DoctorListDTO GetDoctors()
        {
            IEnumerable<DoctorDAO> doctors = doctorsRepository.GetAll();
            DoctorListDTO doctorsListDTO = new DoctorListDTO();
            foreach (DoctorDAO doctor in doctors)
            {
                UserDAO User = usersRepository.Get(doctor.UserId);
                if (User.IsActive)
                {
                    doctorsListDTO.Doctors.Add(new DoctorDTO
                    {
                        Id = doctor.Id,
                        Name = User.FirstName,
                        Surname = User.Surname

                    });
                }
            }
            return doctorsListDTO;
        }
    }
}
