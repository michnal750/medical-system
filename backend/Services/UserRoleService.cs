﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly IRepository<UserRoleDAO, string> rolesRepository;

        public UserRoleService(IRepository<UserRoleDAO, string> rolesRepository)
        {
            this.rolesRepository = rolesRepository;
        }

        public RoleListDTO GetRoles()
        {
            IEnumerable<UserRoleDAO> roles = rolesRepository.GetAll();
            RoleListDTO roleListDTO = new RoleListDTO();
            foreach (UserRoleDAO role in roles)
            {
                roleListDTO.Roles.Add(new RoleDTO
                {
                    Code = role.Code,
                    Name = role.Name
                });
            }
            return roleListDTO;
        }
    }
}
