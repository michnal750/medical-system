﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{
    public class AppointmentStatusService : IAppointmentStatusService
    {
        private readonly IRepository<AppointmentStatusDAO, string> statusesRepository;

        public AppointmentStatusService(IRepository<AppointmentStatusDAO, string> statusesRepository)
        {
            this.statusesRepository = statusesRepository;
        }

        public AppointmentStatusListDTO GetStatuses()
        {
            IEnumerable<AppointmentStatusDAO> statuses = statusesRepository.GetAll();
            AppointmentStatusListDTO appointmentStatusListDTO = new AppointmentStatusListDTO();
            foreach (AppointmentStatusDAO status in statuses)
            {
                appointmentStatusListDTO.AppointmentStatuses.Add(new AppointmentStatusDTO
                {
                    Code = status.Code,
                    Name = status.Name
                });
            }
            return appointmentStatusListDTO;
        }

        public string GetCodeByName(string statusName)
        {
            IEnumerable<AppointmentStatusDAO> appointmentStatuses = statusesRepository.GetAll();
            foreach (AppointmentStatusDAO appointmentStatus in appointmentStatuses)
            {
                if (appointmentStatus.Name.Equals(statusName))
                {
                    return appointmentStatus.Code;
                }
            }
            throw new MedicalSystemServerException("Bledna nazwa statusu wizyty");
        }
    }
}
