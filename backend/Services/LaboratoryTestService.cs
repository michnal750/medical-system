﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MedicalSystemBackend.Services
{
    public class LaboratoryTestService : ILaboratoryTestService
    {
        private readonly IRepository<LaboratoryTestDAO, int> laboratoryTestsRepository;
        private readonly IRepository<LaboratoryTestStatusDAO, string> laboratoryTestsStatusRepository;
        private readonly IRepository<UserDAO, int> usersRepository;
        private readonly IRepository<AppointmentDAO, int> appointmentsRepository;
        private readonly IRepository<PatientDAO, int> patientsRepository;

        public LaboratoryTestService(IRepository<LaboratoryTestDAO, int> laboratoryTestsRepository,
            IRepository<UserDAO, int> usersRepository,
            IRepository<PatientDAO, int> patientsRepository,
            IRepository<AppointmentDAO, int> appointmentsRepository,
            IRepository<LaboratoryTestStatusDAO, string> laboratoryTestsStatusRepository)
        {
            this.appointmentsRepository = appointmentsRepository;
            this.laboratoryTestsRepository = laboratoryTestsRepository;
            this.usersRepository = usersRepository;
            this.patientsRepository = patientsRepository;
            this.laboratoryTestsStatusRepository = laboratoryTestsStatusRepository;
        }

        public LaboratoryTestListDTO GetLaboratoryTests()
        {
            IEnumerable<LaboratoryTestDAO> laboratoryTests = laboratoryTestsRepository.GetAll();
            LaboratoryTestListDTO laboratoryTestsDTO = new LaboratoryTestListDTO();
            string? ExecutionCancelledDate;
            string? AcceptionCancelledDate;
            foreach (LaboratoryTestDAO laboratoryTest in laboratoryTests)
            {
                ExecutionCancelledDate = laboratoryTest.ExecutionCancelledDate != null ? ((DateTime)laboratoryTest.ExecutionCancelledDate).ToString("yyyy-MM-dd") : null;
                AcceptionCancelledDate = laboratoryTest.AcceptionCancelledDate != null ? ((DateTime)laboratoryTest.AcceptionCancelledDate).ToString("yyyy-MM-dd") : null;
                laboratoryTestsDTO.LaboratoryTests.Add(new LaboratoryTestDTO
                {
                    Id = laboratoryTest.Id,
                    DoctorNote = laboratoryTest.DoctorNote,
                    SupervisorNote = laboratoryTest.SupervisorNote,
                    OrderedDate = laboratoryTest.OrderedDate.ToString("yyyy-MM-dd"),
                    AppointmentId = laboratoryTest.AppointmentId,
                    ExaminationCode = laboratoryTest.ExaminationCode,
                    ExecutionCancelledDate = ExecutionCancelledDate,
                    AcceptionCancelledDate = AcceptionCancelledDate,
                    Result = laboratoryTest.Result,
                    LaboratoryAssistantId = laboratoryTest.LaboratoryAssistantId,
                    LaboratorySupervisorId = laboratoryTest.LaboratorySupervisorId,
                    StatusCode = laboratoryTest.StatusCode
                });

            }
            return laboratoryTestsDTO;
        }

        public LaboratoryTestListDTO GetLaboratoryTestsByStatus(string status)
        {
            IEnumerable<LaboratoryTestDAO> laboratoryTests = laboratoryTestsRepository.GetAll();
            LaboratoryTestListDTO laboratoryTestsDTO = new LaboratoryTestListDTO();
            string? ExecutionCancelledDate;
            string? AcceptionCancelledDate;
            foreach (LaboratoryTestDAO laboratoryTest in laboratoryTests)
            {
                if (laboratoryTest.StatusCode == status)
                {
                    ExecutionCancelledDate = laboratoryTest.ExecutionCancelledDate != null ? ((DateTime)laboratoryTest.ExecutionCancelledDate).ToString("yyyy-MM-dd") : null;
                    AcceptionCancelledDate = laboratoryTest.AcceptionCancelledDate != null ? ((DateTime)laboratoryTest.AcceptionCancelledDate).ToString("yyyy-MM-dd") : null;
                    laboratoryTestsDTO.LaboratoryTests.Add(new LaboratoryTestDTO
                    {
                        Id = laboratoryTest.Id,
                        DoctorNote = laboratoryTest.DoctorNote,
                        SupervisorNote = laboratoryTest.SupervisorNote,
                        OrderedDate = laboratoryTest.OrderedDate.ToString("yyyy-MM-dd"),
                        AppointmentId = laboratoryTest.AppointmentId,
                        ExaminationCode = laboratoryTest.ExaminationCode,
                        ExecutionCancelledDate = ExecutionCancelledDate,
                        AcceptionCancelledDate = AcceptionCancelledDate,
                        Result = laboratoryTest.Result,
                        LaboratoryAssistantId = laboratoryTest.LaboratoryAssistantId,
                        LaboratorySupervisorId = laboratoryTest.LaboratorySupervisorId,
                        StatusCode = laboratoryTest.StatusCode
                    });
                }
            }
            return laboratoryTestsDTO;
        }


        public void AddLaboratoryTest(LaboratoryTestDTO newLaboratoryTestDTO)
        {
            laboratoryTestsRepository.Add(new LaboratoryTestDAO
            {
                DoctorNote = newLaboratoryTestDTO.DoctorNote,
                OrderedDate = DateTime.ParseExact(newLaboratoryTestDTO.OrderedDate, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                AppointmentId = newLaboratoryTestDTO.AppointmentId,
                ExaminationCode = newLaboratoryTestDTO.ExaminationCode,
                StatusCode = "ZLE"
            });
        }

        public LaboratoryTestDTO GetLaboratoryTestById(int testId)
        {
            LaboratoryTestDAO laboratoryTest = laboratoryTestsRepository.Get(testId);
            LaboratoryTestDTO laboratoryTestDTO = new LaboratoryTestDTO
            {

                Id = laboratoryTest.Id,
                DoctorNote = laboratoryTest.DoctorNote,
                OrderedDate = laboratoryTest.OrderedDate.ToString("yyyy-MM-dd"),
                AppointmentId = laboratoryTest.AppointmentId,
                ExaminationCode = laboratoryTest.ExaminationCode,
                ExecutionCancelledDate = ((DateTime)laboratoryTest.ExecutionCancelledDate).ToString("yyyy-MM-dd"),
                Result = laboratoryTest.Result,
                LaboratoryAssistantId = (int)laboratoryTest.LaboratoryAssistantId
            };
            return laboratoryTestDTO;
        }
        public void MakeCancelLaboratoryTest(int testId, MakeAcceptCancelLaboratoryTestDTO makelaboratoryTestDTO, int laboratoryAssistantId, string status)
        {
            LaboratoryTestDAO currentLaboratoryTest = laboratoryTestsRepository.Get(testId);

            laboratoryTestsRepository.Update(new LaboratoryTestDAO
            {
                Id = currentLaboratoryTest.Id,
                DoctorNote = currentLaboratoryTest.DoctorNote,
                OrderedDate = currentLaboratoryTest.OrderedDate,
                Result = makelaboratoryTestDTO.Note,
                ExecutionCancelledDate = DateTime.Now,
                SupervisorNote = currentLaboratoryTest.SupervisorNote,
                AcceptionCancelledDate = currentLaboratoryTest.AcceptionCancelledDate,
                StatusCode = status,
                AppointmentId = currentLaboratoryTest.AppointmentId,
                ExaminationCode = currentLaboratoryTest.ExaminationCode,
                LaboratoryAssistantId = laboratoryAssistantId,
                LaboratorySupervisorId = currentLaboratoryTest.LaboratorySupervisorId,

            });
        }

        public void AcceptCancelLaboratoryTest(int testId, MakeAcceptCancelLaboratoryTestDTO acceptLaboratoryTestDTO, int laboratorySupervisorId, string status)
        {
            LaboratoryTestDAO currentLaboratoryTest = laboratoryTestsRepository.Get(testId);

            laboratoryTestsRepository.Update(new LaboratoryTestDAO
            {
                Id = currentLaboratoryTest.Id,
                DoctorNote = currentLaboratoryTest.DoctorNote,
                OrderedDate = currentLaboratoryTest.OrderedDate,
                Result = currentLaboratoryTest.Result,
                ExecutionCancelledDate = currentLaboratoryTest.ExecutionCancelledDate,
                SupervisorNote = acceptLaboratoryTestDTO.Note,
                AcceptionCancelledDate = DateTime.Now,
                StatusCode = status,
                AppointmentId = currentLaboratoryTest.AppointmentId,
                ExaminationCode = currentLaboratoryTest.ExaminationCode,
                LaboratoryAssistantId = currentLaboratoryTest.LaboratoryAssistantId,
                LaboratorySupervisorId = laboratorySupervisorId,

            });

        }

        public LaboratoryTestListDTO GetLaboratoryTestByPatientId(int patientId)
        {

            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            IEnumerable<LaboratoryTestDAO> laboratoryTests = laboratoryTestsRepository.GetAll();
            LaboratoryTestListDTO laboratoryTestsDTO = new LaboratoryTestListDTO();
            string? ExecutionCancelledDate;
            string? AcceptionCancelledDate;
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.PatientId == patientId)
                {

                    foreach (LaboratoryTestDAO laboratoryTest in laboratoryTests)
                    {
                        if (laboratoryTest.AppointmentId == appointment.Id)
                        {
                            ExecutionCancelledDate = laboratoryTest.ExecutionCancelledDate != null ? ((DateTime)laboratoryTest.ExecutionCancelledDate).ToString("yyyy-MM-dd") : null;
                            AcceptionCancelledDate = laboratoryTest.AcceptionCancelledDate != null ? ((DateTime)laboratoryTest.AcceptionCancelledDate).ToString("yyyy-MM-dd") : null;
                            laboratoryTestsDTO.LaboratoryTests.Add(new LaboratoryTestDTO
                            {

                                Id = laboratoryTest.Id,
                                DoctorNote = laboratoryTest.DoctorNote,
                                SupervisorNote = laboratoryTest.SupervisorNote,
                                OrderedDate = laboratoryTest.OrderedDate.ToString("yyyy-MM-dd"),
                                AppointmentId = laboratoryTest.AppointmentId,
                                ExaminationCode = laboratoryTest.ExaminationCode,
                                ExecutionCancelledDate = ExecutionCancelledDate,
                                AcceptionCancelledDate = AcceptionCancelledDate,
                                Result = laboratoryTest.Result,
                                LaboratoryAssistantId = laboratoryTest.LaboratoryAssistantId,
                                LaboratorySupervisorId = laboratoryTest.LaboratorySupervisorId,
                                StatusCode = laboratoryTest.StatusCode
                            });


                        }
                    }
                }
            }

            return laboratoryTestsDTO;
        }

    }




}