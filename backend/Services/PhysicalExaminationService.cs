﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{

    public class PhysicalExaminationService : IPhysicalExaminationService
    {
        private readonly IRepository<PhysicalExaminationDAO, int> physicalExaminationRepository;
        private readonly IRepository<AppointmentDAO, int> appointmentRepository;

        public PhysicalExaminationService(
            IRepository<PhysicalExaminationDAO, int> physicalExaminationRepository,
            IRepository<AppointmentDAO, int> appointmentRepository)
        {
            this.physicalExaminationRepository = physicalExaminationRepository;
            this.appointmentRepository = appointmentRepository;
        }

        public void AddPhysicalExamination(PhysicalExaminationDTO newPhysicalExaminationDTO)
        {
            physicalExaminationRepository.Add(new PhysicalExaminationDAO
            {
                Id = newPhysicalExaminationDTO.Id,
                AppointmentId = newPhysicalExaminationDTO.AppointmentId,
                Result = newPhysicalExaminationDTO.Result,
                ExaminationCode = newPhysicalExaminationDTO.ExaminationCode
            });
        }
        public PhysicalExaminationListDTO GetPhysicalExaminationsByPatientId(int patientId)
        {
            PhysicalExaminationListDTO physicalExaminationList = new PhysicalExaminationListDTO();
            IEnumerable<PhysicalExaminationDAO> physicalExaminations = physicalExaminationRepository.GetAll();
            foreach (PhysicalExaminationDAO physicalExamination in physicalExaminations)
            {
                if (appointmentRepository.Get(physicalExamination.AppointmentId).PatientId == patientId)
                {
                    AppointmentDAO appointment = appointmentRepository.Get(physicalExamination.AppointmentId);
                    physicalExaminationList.physicalExaminations.Add(new PhysicalExaminationDTO
                    {
                        Id = physicalExamination.Id,
                        Result = physicalExamination.Result,
                        AppointmentId = physicalExamination.AppointmentId,
                        FinishedCancelledDate = appointment.FinishedCancelledDate.ToString(),
                        RegistrationDate = appointment.RegistrationDate.ToString(),
                        Code = physicalExamination.ExaminationCode
                    });
                }
            }
            return physicalExaminationList;
        }
    }
}
